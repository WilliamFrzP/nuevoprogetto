                      Relazione Tecnica del progetto:

Autore: William Fernandez
Data e Luogo: 21/10/19 Torino

E' stato richiesto la costruzione di un'applicativo che mostri una vista prodotti e un'anteprima dello scontrino.
L'applicativo deve gestire i dati forniti attraverso le chiamate ad API e deve adattarsi a schermi di diverse dimensioni.
Questo programma sarà utilizzato per controllare la quantità e il prezzo dei prodotti scelti.
Per la realizzazione di questo progetto, ho utilizzato il framework VueJS, lo state manager Vuex, il framework Bootstrap e il linguaggio di stile css.
Il progetto è stato pubblicato su Gitlab.
Al progetto li manca alcune richieste del cliente.
Il progetto si trova nel seguente link:
https://gitlab.com/WilliamFrzP/nuevoprogetto

Conclusione: Ho voluto programmare questo progetto con la tecnologia VueJS, come richiesto nella descrizione del test.
Non sono riuscito a completare il test perche la tecnologia che ho utilizzato era nuova per me e ho trovato difficoltà ad impararla in poco tempo, nonostante ciò sono riuscito a programmare una parte del progetto.
